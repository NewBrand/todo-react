import React from 'react';

import './styles.css';

const TaskInput = ({addTask}) => {
    let input;
    const handleAddTask = event =>{
        event.preventDefault();
        if(input.value !== ''){
            addTask(input.value);
            input.value = '';
        }
    };
    return (
        <form onSubmit={handleAddTask}>
            <input ref={node=>{input = node}} type="text"/>
            <button type="submit">add</button>
        </form>
    );
};
export default TaskInput;