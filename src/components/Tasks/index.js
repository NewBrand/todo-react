import React, {Component} from 'react';
import TaskItem from './TaskItem';
import TaskInput from './TaskInput';
import './styles.css';

class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        };
        this.handleAddTask = this.handleAddTask.bind(this);
        this.handleDoneTask = this.handleDoneTask.bind(this);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
    }

    handleAddTask(value) {
        const task = {value: value, isDone: false};
        this.state.tasks.push(task);
        this.setState({tasks: this.state.tasks})

    }
    handleDoneTask(id){
        let tasks = this.state.tasks;
        tasks[id].isDone = !tasks[id].isDone;
        this.setState({tasks: tasks});
    }
    handleDeleteTask(id){
        let tasks = this.state.tasks;
        tasks.splice(id,1);
        this.setState({tasks: tasks});
    }
    renderList(){
        return this.state.tasks.map((task, index)=>{
            task.id = index;
            return <li key={index}><TaskItem done={this.handleDoneTask} remove={this.handleDeleteTask} task={task}/></li>
        })
    }
    render() {
        return (
            <div className="tasks">
                <ul className="tasks">
                    {this.renderList()}
                </ul>
                <TaskInput addTask={this.handleAddTask}/>
            </div>

        );
    }
}

export default Tasks;
