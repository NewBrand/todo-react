import React from 'react';
import './styles.css';

const TaskItem = ({task, remove, done}) => {

    const handleDone = () => {
        done(task.id);
    };
    const handleRemove = () => {
        remove(task.id);
    };
    return (
        <div>
            <p>{task.value}</p>
            <button onClick={handleDone}>done</button>
            <button onClick={handleRemove}>delete</button>
        </div>
    );
};


export default TaskItem;
